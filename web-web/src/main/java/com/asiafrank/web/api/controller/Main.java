package com.asiafrank.web.api.controller;

import org.apache.commons.codec.binary.Base64;

import java.io.*;

/**
 * Created by Xiaofan Zhang on 6/5/2016.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        String fileName = "/Users/asiafrank/Desktop/before.txt";
        BufferedReader in = new BufferedReader(new FileReader(fileName));
        String s;
        String base64Str = "";
        while ((s = in.readLine()) != null) {
            base64Str += s;
        }
        in.close();

        base64Str = base64Str.replaceAll(" ", "+");
        base64Str = base64Str.replaceAll("\\r\\n", "");
        byte[] bytes = Base64.decodeBase64(base64Str);
        FileOutputStream out = new FileOutputStream("/Users/asiafrank/Desktop/src2.jpg");
        out.write(bytes);
        out.close();
    }
}
