package com.asiafrank.web.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.asiafrank.web.bo.PermissionBO;
import com.asiafrank.web.model.Permission;
import com.calanger.common.web.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * For Test page.
 * Created by Xiaofan Zhang | 2015-09-30
 */
@Controller
@RequestMapping("/test")
public class TestController extends BaseController {

    @Autowired
    private PermissionBO permissionBO;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        return "/test";
    }

    @RequestMapping(value = "/json", method = RequestMethod.GET)
    public String jsonTest(Model model) {
        return "/test";
    }

    @RequestMapping(value = "/permissions", method = RequestMethod.GET)
    public void permissions(HttpServletResponse response) {
        List<Permission> permissions = permissionBO.find();
        ResponseUtils.writeJsonArray(response, permissions); // 驼峰式
    }

    @RequestMapping(value = "/json-2", method = RequestMethod.GET)
    public void json(HttpServletResponse response) {
        JSONObject json = new JSONObject();
        json.put("code", 0);
        ResponseUtils.writeJsonArray(response, json); // 驼峰式
    }

    @RequestMapping(value = "/get-permission", method = RequestMethod.GET)
    @ResponseBody
    public Permission getPermission(HttpServletResponse response) {
        Permission p = permissionBO.get(1);
        return p; // 驼峰式
    }
}
