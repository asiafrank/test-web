package com.asiafrank.web.api.controller;

import com.asiafrank.web.bo.PermissionBO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Created by Administrator on 2015/8/25.
 */
@Controller
public class IndexController extends BaseController {
    /*
                       _ooOoo_
                      o8888888o
                      88" . "88
                      (| -_- |)
                      O\  =  /O
                   ____/`---'\____
                 .'  \\|     |//  `.
                /  \\|||  :  |||//  \
               /  _||||| -:- |||||-  \
               |   | \\\  -  /// |   |
               | \_|  ''\---/''  |   |
               \  .-\__  `-`  ___/-. /
             ___`. .'  /--.--\  `. . __
          ."" '<  `.___\_<|>_/___.'  >'"".
         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
         \  \ `-.   \_ __\ /__ _/   .-` /  /
    ======`-.____`-.___\_____/___.-`____.-'======
                       `=---='
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
               佛祖保佑       永无BUG
    */
    public final static Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    private PermissionBO permissionBO;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        // get all permission recode
        try {

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Permission load error: ", e);
        }
        return "/index";
    }
}
