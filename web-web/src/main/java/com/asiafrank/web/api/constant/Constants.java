package com.asiafrank.web.api.constant;

import com.calanger.common.util.Config;

public final class Constants {
    public static final String BASE_DOMAIN = Config.getConfig().get("base.domain");
    public static final String STATIC_DOMAIN = Config.getConfig().get("static.domain");
    public static final String HOME_DOMAIN = Config.getConfig().get("home.domain");
    public static final String SERVICE_DOMAIN = Config.getConfig().get("service.domain");

    private Constants() {
    }
}
