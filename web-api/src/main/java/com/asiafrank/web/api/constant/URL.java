package com.asiafrank.web.api.constant;

public final class URL {
    public static final String VERSION = "/v1";
    public static final String USER = "/user";

    private URL() {
    }
}
