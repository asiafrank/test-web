package com.asiafrank.web.api.wrapper;

import com.asiafrank.web.api.constant.STATUS;

public class ResultWrapper {
    private String code;
    private String msg;

    public ResultWrapper(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private ResultWrapper(STATUS status) {
        this.code = status.getCode();
        this.msg = status.getMsg();
    }

    public static ResultWrapper newInstance(STATUS status) {
        return new ResultWrapper(status);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
