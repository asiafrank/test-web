package com.asiafrank.web.api.wrapper;

public class ObjWrapper extends ResultWrapper {
    private java.lang.Object data;
    public ObjWrapper(String code, String msg, java.lang.Object data) {
        super(code, msg);
        this.data = data;
    }

    public java.lang.Object getData() {
        return data;
    }

    public void setData(java.lang.Object data) {
        this.data = data;
    }
}
