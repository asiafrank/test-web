package com.asiafrank.web.api.constant;

public enum STATUS {
    SUCCESS("000", "success"),
    FAIL("001", "fail");

    private String code;
    private String msg;
    STATUS(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
