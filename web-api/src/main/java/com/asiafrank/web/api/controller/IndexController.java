package com.asiafrank.web.api.controller;

import com.asiafrank.web.api.base.BaseController;
import com.asiafrank.web.api.constant.STATUS;
import com.asiafrank.web.api.constant.URL;
import com.asiafrank.web.api.wrapper.ResultWrapper;
import com.calanger.common.web.util.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;


/**
 * Created by Administrator on 2015/8/25.
 */
@Controller
@RequestMapping(URL.VERSION)
public class IndexController extends BaseController {

    public final static Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @RequestMapping(value = URL.USER, method = RequestMethod.GET)
    public void getUser(HttpServletResponse response) {
        // do something
        ResponseUtils.writeJsonObject(response, ResultWrapper.newInstance(STATUS.SUCCESS));
//        ResponseUtils.writeJsonObject(response, ObjectWrapper.newInstance(STATUS.SUCCESS, data));
    }
}
