package com.asiafrank.web.model;

import java.io.Serializable;

public class OperatorRole implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.lang.Integer id;
    private java.lang.Integer operatorId;
    private java.lang.Integer roleId;

    public java.lang.Integer getId() {
        return id;
    }

    public void setId(java.lang.Integer id) {
        this.id = id;
    }

    public java.lang.Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(java.lang.Integer operatorId) {
        this.operatorId = operatorId;
    }

    public java.lang.Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(java.lang.Integer roleId) {
        this.roleId = roleId;
    }
}
