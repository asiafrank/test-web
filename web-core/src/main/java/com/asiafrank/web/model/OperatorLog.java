package com.asiafrank.web.model;

import java.io.Serializable;

public class OperatorLog implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.lang.Integer id;
    private java.lang.Integer operatorId;
    private java.lang.String operatorUsername;
    private java.lang.String operatorIp;
    private java.lang.Integer operationType;
    private java.lang.String operationTarget;
    private java.lang.String operationContent;
    private java.util.Date createdAt;

    public java.lang.Integer getId() {
        return id;
    }

    public void setId(java.lang.Integer id) {
        this.id = id;
    }

    public java.lang.Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(java.lang.Integer operatorId) {
        this.operatorId = operatorId;
    }

    public java.lang.String getOperatorUsername() {
        return operatorUsername;
    }

    public void setOperatorUsername(java.lang.String operatorUsername) {
        this.operatorUsername = operatorUsername;
    }

    public java.lang.String getOperatorIp() {
        return operatorIp;
    }

    public void setOperatorIp(java.lang.String operatorIp) {
        this.operatorIp = operatorIp;
    }

    public java.lang.Integer getOperationType() {
        return operationType;
    }

    public void setOperationType(java.lang.Integer operationType) {
        this.operationType = operationType;
    }

    public java.lang.String getOperationTarget() {
        return operationTarget;
    }

    public void setOperationTarget(java.lang.String operationTarget) {
        this.operationTarget = operationTarget;
    }

    public java.lang.String getOperationContent() {
        return operationContent;
    }

    public void setOperationContent(java.lang.String operationContent) {
        this.operationContent = operationContent;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }
}
