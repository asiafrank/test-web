package com.asiafrank.web.model;

import java.io.Serializable;

public class Resource implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.lang.Integer id;
    private java.lang.String name;
    private java.lang.String showName;
    private java.lang.Integer parentId;
    private java.lang.Integer permissionId;
    private java.lang.Integer resourceType;
    private java.lang.String baseUrl;
    private java.lang.Integer httpMethod;
    private java.lang.Integer sortOrder;
    private java.util.Date createdAt;
    private java.util.Date updatedAt;

    public java.lang.Integer getId() {
        return id;
    }

    public void setId(java.lang.Integer id) {
        this.id = id;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getShowName() {
        return showName;
    }

    public void setShowName(java.lang.String showName) {
        this.showName = showName;
    }

    public java.lang.Integer getParentId() {
        return parentId;
    }

    public void setParentId(java.lang.Integer parentId) {
        this.parentId = parentId;
    }

    public java.lang.Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(java.lang.Integer permissionId) {
        this.permissionId = permissionId;
    }

    public java.lang.Integer getResourceType() {
        return resourceType;
    }

    public void setResourceType(java.lang.Integer resourceType) {
        this.resourceType = resourceType;
    }

    public java.lang.String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(java.lang.String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public java.lang.Integer getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(java.lang.Integer httpMethod) {
        this.httpMethod = httpMethod;
    }

    public java.lang.Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(java.lang.Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
