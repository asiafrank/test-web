package com.asiafrank.web.model;

import java.io.Serializable;

public class OperatorPermission implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.lang.Integer id;
    private java.lang.Integer operatorId;
    private java.lang.Integer permissionId;

    public java.lang.Integer getId() {
        return id;
    }

    public void setId(java.lang.Integer id) {
        this.id = id;
    }

    public java.lang.Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(java.lang.Integer operatorId) {
        this.operatorId = operatorId;
    }

    public java.lang.Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(java.lang.Integer permissionId) {
        this.permissionId = permissionId;
    }
}
