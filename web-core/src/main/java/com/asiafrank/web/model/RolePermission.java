package com.asiafrank.web.model;

import java.io.Serializable;

public class RolePermission implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.lang.Integer id;
    private java.lang.Integer roleId;
    private java.lang.Integer permissionId;

    public java.lang.Integer getId() {
        return id;
    }

    public void setId(java.lang.Integer id) {
        this.id = id;
    }

    public java.lang.Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(java.lang.Integer roleId) {
        this.roleId = roleId;
    }

    public java.lang.Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(java.lang.Integer permissionId) {
        this.permissionId = permissionId;
    }
}
