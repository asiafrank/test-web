package com.asiafrank.web.model;

import com.google.common.collect.Lists;

import java.util.List;

public class PermissionInfo extends Permission {

    private List<PermissionInfo> children;

    public PermissionInfo() {
        this.children = Lists.newArrayList();
    }

    public PermissionInfo(Permission permission) {
        this.setId(permission.getId());
        this.setName(permission.getName());
        this.setShowName(permission.getShowName());
        this.setParentId(permission.getParentId());
        this.setSortOrder(permission.getSortOrder());
        this.setCreatedAt(permission.getCreatedAt());
        this.setUpdatedAt(permission.getUpdatedAt());
        this.children = Lists.newArrayList();
    }

    public List<PermissionInfo> getChildren() {
        return children;
    }

    public void setChildren(List<PermissionInfo> children) {
        this.children = children;
    }

    public void addChild(PermissionInfo permissionInfo) {
        this.children.add(permissionInfo);
    }

}
