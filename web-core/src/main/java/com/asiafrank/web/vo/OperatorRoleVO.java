package com.asiafrank.web.vo;

import java.util.ArrayList;
import java.util.List;

import com.calanger.common.dao.Expression;
import com.calanger.common.dao.ExpressionChain;
import com.asiafrank.web.model.OperatorRole;

public class OperatorRoleVO extends OperatorRole {
    private static final long serialVersionUID = 1L;

    private List<ExpressionChain> expressionChainList;

    public OperatorRoleVO() {
        expressionChainList = new ArrayList<ExpressionChain>();
    }

    public OperatorRoleVO or(ExpressionChain expressionChain) {
        expressionChainList.add(expressionChain);
        return this;
    }

    public OperatorRoleVO or(Expression expression) {
        expressionChainList.add(new ExpressionChain().and(expression));
        return this;
    }

    public OperatorRoleVO and(Expression expression) {
        if (expressionChainList.isEmpty()) {
            expressionChainList.add(new ExpressionChain());
        }
        expressionChainList.get(0).and(expression);
        return this;
    }

    public List<ExpressionChain> getExpressionChainList() {
        return expressionChainList;
    }

    public void setExpressionChainList(List<ExpressionChain> expressionChainList) {
        this.expressionChainList = expressionChainList;
    }
}
