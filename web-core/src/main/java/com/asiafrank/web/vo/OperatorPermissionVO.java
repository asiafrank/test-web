package com.asiafrank.web.vo;

import java.util.ArrayList;
import java.util.List;

import com.calanger.common.dao.Expression;
import com.calanger.common.dao.ExpressionChain;
import com.asiafrank.web.model.OperatorPermission;

public class OperatorPermissionVO extends OperatorPermission {
    private static final long serialVersionUID = 1L;

    private List<ExpressionChain> expressionChainList;

    public OperatorPermissionVO() {
        expressionChainList = new ArrayList<ExpressionChain>();
    }

    public OperatorPermissionVO or(ExpressionChain expressionChain) {
        expressionChainList.add(expressionChain);
        return this;
    }

    public OperatorPermissionVO or(Expression expression) {
        expressionChainList.add(new ExpressionChain().and(expression));
        return this;
    }

    public OperatorPermissionVO and(Expression expression) {
        if (expressionChainList.isEmpty()) {
            expressionChainList.add(new ExpressionChain());
        }
        expressionChainList.get(0).and(expression);
        return this;
    }

    public List<ExpressionChain> getExpressionChainList() {
        return expressionChainList;
    }

    public void setExpressionChainList(List<ExpressionChain> expressionChainList) {
        this.expressionChainList = expressionChainList;
    }
}
