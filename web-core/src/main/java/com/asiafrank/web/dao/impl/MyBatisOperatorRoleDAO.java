package com.asiafrank.web.dao.impl;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.calanger.common.dao.AbstractDAO;
import com.asiafrank.web.dao.OperatorRoleDAO;
import com.asiafrank.web.model.OperatorRole;
import com.asiafrank.web.vo.OperatorRoleVO;

@Repository("operatorRoleDAO")
public class MyBatisOperatorRoleDAO extends AbstractDAO<OperatorRole, OperatorRoleVO, java.lang.Integer> implements OperatorRoleDAO {
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Override
    protected SqlSessionTemplate getSqlSessionTemplate() {
        return sqlSessionTemplate;
    }

    @Override
    protected String getNamespace() {
        return "operatorRoleDAO";
    }
}
