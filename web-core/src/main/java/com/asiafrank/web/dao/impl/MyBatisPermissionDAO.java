package com.asiafrank.web.dao.impl;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.calanger.common.dao.AbstractDAO;
import com.asiafrank.web.dao.PermissionDAO;
import com.asiafrank.web.model.Permission;
import com.asiafrank.web.vo.PermissionVO;

@Repository("permissionDAO")
public class MyBatisPermissionDAO extends AbstractDAO<Permission, PermissionVO, java.lang.Integer> implements PermissionDAO {
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Override
    protected SqlSessionTemplate getSqlSessionTemplate() {
        return sqlSessionTemplate;
    }

    @Override
    protected String getNamespace() {
        return "permissionDAO";
    }
}
