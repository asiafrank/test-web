package com.asiafrank.web.dao.impl;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.calanger.common.dao.AbstractDAO;
import com.asiafrank.web.dao.ResourceDAO;
import com.asiafrank.web.model.Resource;
import com.asiafrank.web.vo.ResourceVO;

@Repository("resourceDAO")
public class MyBatisResourceDAO extends AbstractDAO<Resource, ResourceVO, java.lang.Integer> implements ResourceDAO {
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Override
    protected SqlSessionTemplate getSqlSessionTemplate() {
        return sqlSessionTemplate;
    }

    @Override
    protected String getNamespace() {
        return "resourceDAO";
    }
}
