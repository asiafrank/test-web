package com.asiafrank.web.dao;

import com.calanger.common.dao.DAO;
import com.asiafrank.web.model.RolePermission;
import com.asiafrank.web.vo.RolePermissionVO;

public interface RolePermissionDAO extends DAO<RolePermission, RolePermissionVO, java.lang.Integer> {
}
