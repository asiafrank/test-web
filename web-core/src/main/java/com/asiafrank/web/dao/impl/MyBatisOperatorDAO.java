package com.asiafrank.web.dao.impl;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.calanger.common.dao.AbstractDAO;
import com.asiafrank.web.dao.OperatorDAO;
import com.asiafrank.web.model.Operator;
import com.asiafrank.web.vo.OperatorVO;

@Repository("operatorDAO")
public class MyBatisOperatorDAO extends AbstractDAO<Operator, OperatorVO, java.lang.Integer> implements OperatorDAO {
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Override
    protected SqlSessionTemplate getSqlSessionTemplate() {
        return sqlSessionTemplate;
    }

    @Override
    protected String getNamespace() {
        return "operatorDAO";
    }
}
