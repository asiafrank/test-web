package com.asiafrank.web.dao;

import com.calanger.common.dao.DAO;
import com.asiafrank.web.model.Resource;
import com.asiafrank.web.vo.ResourceVO;

public interface ResourceDAO extends DAO<Resource, ResourceVO, java.lang.Integer> {
}
