package com.asiafrank.web.dao;

import com.calanger.common.dao.DAO;
import com.asiafrank.web.model.OperatorRole;
import com.asiafrank.web.vo.OperatorRoleVO;

public interface OperatorRoleDAO extends DAO<OperatorRole, OperatorRoleVO, java.lang.Integer> {
}
