package com.asiafrank.web.dao;

import com.calanger.common.dao.DAO;
import com.asiafrank.web.model.OperatorLog;
import com.asiafrank.web.vo.OperatorLogVO;

public interface OperatorLogDAO extends DAO<OperatorLog, OperatorLogVO, java.lang.Integer> {
}
