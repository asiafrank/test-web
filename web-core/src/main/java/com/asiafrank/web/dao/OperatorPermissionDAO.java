package com.asiafrank.web.dao;

import com.calanger.common.dao.DAO;
import com.asiafrank.web.model.OperatorPermission;
import com.asiafrank.web.vo.OperatorPermissionVO;

public interface OperatorPermissionDAO extends DAO<OperatorPermission, OperatorPermissionVO, java.lang.Integer> {
}
