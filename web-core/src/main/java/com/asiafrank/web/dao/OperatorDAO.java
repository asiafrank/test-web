package com.asiafrank.web.dao;

import com.calanger.common.dao.DAO;
import com.asiafrank.web.model.Operator;
import com.asiafrank.web.vo.OperatorVO;

public interface OperatorDAO extends DAO<Operator, OperatorVO, java.lang.Integer> {
}
