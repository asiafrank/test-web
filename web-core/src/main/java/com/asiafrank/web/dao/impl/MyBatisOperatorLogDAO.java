package com.asiafrank.web.dao.impl;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.calanger.common.dao.AbstractDAO;
import com.asiafrank.web.dao.OperatorLogDAO;
import com.asiafrank.web.model.OperatorLog;
import com.asiafrank.web.vo.OperatorLogVO;

@Repository("operatorLogDAO")
public class MyBatisOperatorLogDAO extends AbstractDAO<OperatorLog, OperatorLogVO, java.lang.Integer> implements OperatorLogDAO {
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Override
    protected SqlSessionTemplate getSqlSessionTemplate() {
        return sqlSessionTemplate;
    }

    @Override
    protected String getNamespace() {
        return "operatorLogDAO";
    }
}
