package com.asiafrank.web.dao;

import com.calanger.common.dao.DAO;
import com.asiafrank.web.model.Role;
import com.asiafrank.web.vo.RoleVO;

public interface RoleDAO extends DAO<Role, RoleVO, java.lang.Integer> {
}
