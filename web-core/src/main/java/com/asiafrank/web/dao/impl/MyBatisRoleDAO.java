package com.asiafrank.web.dao.impl;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.calanger.common.dao.AbstractDAO;
import com.asiafrank.web.dao.RoleDAO;
import com.asiafrank.web.model.Role;
import com.asiafrank.web.vo.RoleVO;

@Repository("roleDAO")
public class MyBatisRoleDAO extends AbstractDAO<Role, RoleVO, java.lang.Integer> implements RoleDAO {
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Override
    protected SqlSessionTemplate getSqlSessionTemplate() {
        return sqlSessionTemplate;
    }

    @Override
    protected String getNamespace() {
        return "roleDAO";
    }
}
