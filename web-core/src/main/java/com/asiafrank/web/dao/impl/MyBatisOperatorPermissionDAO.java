package com.asiafrank.web.dao.impl;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.calanger.common.dao.AbstractDAO;
import com.asiafrank.web.dao.OperatorPermissionDAO;
import com.asiafrank.web.model.OperatorPermission;
import com.asiafrank.web.vo.OperatorPermissionVO;

@Repository("operatorPermissionDAO")
public class MyBatisOperatorPermissionDAO extends AbstractDAO<OperatorPermission, OperatorPermissionVO, java.lang.Integer> implements OperatorPermissionDAO {
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Override
    protected SqlSessionTemplate getSqlSessionTemplate() {
        return sqlSessionTemplate;
    }

    @Override
    protected String getNamespace() {
        return "operatorPermissionDAO";
    }
}
