package com.asiafrank.web.dao;

import com.calanger.common.dao.DAO;
import com.asiafrank.web.model.Permission;
import com.asiafrank.web.vo.PermissionVO;

public interface PermissionDAO extends DAO<Permission, PermissionVO, java.lang.Integer> {
}
