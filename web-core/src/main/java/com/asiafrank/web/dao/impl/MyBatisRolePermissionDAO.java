package com.asiafrank.web.dao.impl;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.calanger.common.dao.AbstractDAO;
import com.asiafrank.web.dao.RolePermissionDAO;
import com.asiafrank.web.model.RolePermission;
import com.asiafrank.web.vo.RolePermissionVO;

@Repository("rolePermissionDAO")
public class MyBatisRolePermissionDAO extends AbstractDAO<RolePermission, RolePermissionVO, java.lang.Integer> implements RolePermissionDAO {
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Override
    protected SqlSessionTemplate getSqlSessionTemplate() {
        return sqlSessionTemplate;
    }

    @Override
    protected String getNamespace() {
        return "rolePermissionDAO";
    }
}
