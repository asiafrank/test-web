package com.asiafrank.web.bo;

import com.calanger.common.bo.BO;
import com.asiafrank.web.model.OperatorRole;
import com.asiafrank.web.vo.OperatorRoleVO;

public interface OperatorRoleBO extends BO<OperatorRole, OperatorRoleVO, java.lang.Integer> {
}
