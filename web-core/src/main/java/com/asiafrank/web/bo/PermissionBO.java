package com.asiafrank.web.bo;

import com.calanger.common.bo.BO;
import com.asiafrank.web.model.Permission;
import com.asiafrank.web.vo.PermissionVO;

public interface PermissionBO extends BO<Permission, PermissionVO, java.lang.Integer> {
}
