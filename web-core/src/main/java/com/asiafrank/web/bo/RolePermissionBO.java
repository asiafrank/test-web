package com.asiafrank.web.bo;

import com.calanger.common.bo.BO;
import com.asiafrank.web.model.RolePermission;
import com.asiafrank.web.vo.RolePermissionVO;

public interface RolePermissionBO extends BO<RolePermission, RolePermissionVO, java.lang.Integer> {
}
