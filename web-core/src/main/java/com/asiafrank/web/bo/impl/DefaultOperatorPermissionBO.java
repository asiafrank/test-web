package com.asiafrank.web.bo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calanger.common.bo.AbstractBO;
import com.calanger.common.dao.DAO;
import com.asiafrank.web.bo.OperatorPermissionBO;
import com.asiafrank.web.dao.OperatorPermissionDAO;
import com.asiafrank.web.model.OperatorPermission;
import com.asiafrank.web.vo.OperatorPermissionVO;

@Service("operatorPermissionBO")
public class DefaultOperatorPermissionBO extends AbstractBO<OperatorPermission, OperatorPermissionVO, java.lang.Integer> implements OperatorPermissionBO {
    @Autowired
    private OperatorPermissionDAO operatorPermissionDAO;

    @Override
    protected DAO<OperatorPermission, OperatorPermissionVO, java.lang.Integer> getDAO() {
        return operatorPermissionDAO;
    }
}
