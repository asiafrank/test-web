package com.asiafrank.web.bo;

import com.calanger.common.bo.BO;
import com.asiafrank.web.model.OperatorPermission;
import com.asiafrank.web.vo.OperatorPermissionVO;

public interface OperatorPermissionBO extends BO<OperatorPermission, OperatorPermissionVO, java.lang.Integer> {
}
