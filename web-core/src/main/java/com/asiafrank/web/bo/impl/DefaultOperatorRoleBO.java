package com.asiafrank.web.bo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calanger.common.bo.AbstractBO;
import com.calanger.common.dao.DAO;
import com.asiafrank.web.bo.OperatorRoleBO;
import com.asiafrank.web.dao.OperatorRoleDAO;
import com.asiafrank.web.model.OperatorRole;
import com.asiafrank.web.vo.OperatorRoleVO;

@Service("operatorRoleBO")
public class DefaultOperatorRoleBO extends AbstractBO<OperatorRole, OperatorRoleVO, java.lang.Integer> implements OperatorRoleBO {
    @Autowired
    private OperatorRoleDAO operatorRoleDAO;

    @Override
    protected DAO<OperatorRole, OperatorRoleVO, java.lang.Integer> getDAO() {
        return operatorRoleDAO;
    }
}
