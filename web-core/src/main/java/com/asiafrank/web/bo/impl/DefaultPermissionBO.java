package com.asiafrank.web.bo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calanger.common.bo.AbstractBO;
import com.calanger.common.dao.DAO;
import com.asiafrank.web.bo.PermissionBO;
import com.asiafrank.web.dao.PermissionDAO;
import com.asiafrank.web.model.Permission;
import com.asiafrank.web.vo.PermissionVO;

@Service("permissionBO")
public class DefaultPermissionBO extends AbstractBO<Permission, PermissionVO, java.lang.Integer> implements PermissionBO {
    @Autowired
    private PermissionDAO permissionDAO;

    @Override
    protected DAO<Permission, PermissionVO, java.lang.Integer> getDAO() {
        return permissionDAO;
    }
}
