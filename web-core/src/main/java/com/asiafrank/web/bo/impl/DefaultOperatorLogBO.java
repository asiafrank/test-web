package com.asiafrank.web.bo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calanger.common.bo.AbstractBO;
import com.calanger.common.dao.DAO;
import com.asiafrank.web.bo.OperatorLogBO;
import com.asiafrank.web.dao.OperatorLogDAO;
import com.asiafrank.web.model.OperatorLog;
import com.asiafrank.web.vo.OperatorLogVO;

@Service("operatorLogBO")
public class DefaultOperatorLogBO extends AbstractBO<OperatorLog, OperatorLogVO, java.lang.Integer> implements OperatorLogBO {
    @Autowired
    private OperatorLogDAO operatorLogDAO;

    @Override
    protected DAO<OperatorLog, OperatorLogVO, java.lang.Integer> getDAO() {
        return operatorLogDAO;
    }
}
