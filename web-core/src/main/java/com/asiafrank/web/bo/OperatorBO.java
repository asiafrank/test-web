package com.asiafrank.web.bo;

import com.calanger.common.bo.BO;
import com.asiafrank.web.model.Operator;
import com.asiafrank.web.vo.OperatorVO;

public interface OperatorBO extends BO<Operator, OperatorVO, java.lang.Integer> {
}
