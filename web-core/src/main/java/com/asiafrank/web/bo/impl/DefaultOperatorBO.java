package com.asiafrank.web.bo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calanger.common.bo.AbstractBO;
import com.calanger.common.dao.DAO;
import com.asiafrank.web.bo.OperatorBO;
import com.asiafrank.web.dao.OperatorDAO;
import com.asiafrank.web.model.Operator;
import com.asiafrank.web.vo.OperatorVO;

@Service("operatorBO")
public class DefaultOperatorBO extends AbstractBO<Operator, OperatorVO, java.lang.Integer> implements OperatorBO {
    @Autowired
    private OperatorDAO operatorDAO;

    @Override
    protected DAO<Operator, OperatorVO, java.lang.Integer> getDAO() {
        return operatorDAO;
    }
}
