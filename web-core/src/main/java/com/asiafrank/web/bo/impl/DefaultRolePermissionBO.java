package com.asiafrank.web.bo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calanger.common.bo.AbstractBO;
import com.calanger.common.dao.DAO;
import com.asiafrank.web.bo.RolePermissionBO;
import com.asiafrank.web.dao.RolePermissionDAO;
import com.asiafrank.web.model.RolePermission;
import com.asiafrank.web.vo.RolePermissionVO;

@Service("rolePermissionBO")
public class DefaultRolePermissionBO extends AbstractBO<RolePermission, RolePermissionVO, java.lang.Integer> implements RolePermissionBO {
    @Autowired
    private RolePermissionDAO rolePermissionDAO;

    @Override
    protected DAO<RolePermission, RolePermissionVO, java.lang.Integer> getDAO() {
        return rolePermissionDAO;
    }
}
