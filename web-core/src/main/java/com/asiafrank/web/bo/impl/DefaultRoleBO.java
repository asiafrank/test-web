package com.asiafrank.web.bo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calanger.common.bo.AbstractBO;
import com.calanger.common.dao.DAO;
import com.asiafrank.web.bo.RoleBO;
import com.asiafrank.web.dao.RoleDAO;
import com.asiafrank.web.model.Role;
import com.asiafrank.web.vo.RoleVO;

@Service("roleBO")
public class DefaultRoleBO extends AbstractBO<Role, RoleVO, java.lang.Integer> implements RoleBO {
    @Autowired
    private RoleDAO roleDAO;

    @Override
    protected DAO<Role, RoleVO, java.lang.Integer> getDAO() {
        return roleDAO;
    }
}
