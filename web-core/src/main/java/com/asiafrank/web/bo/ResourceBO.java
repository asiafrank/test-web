package com.asiafrank.web.bo;

import com.calanger.common.bo.BO;
import com.asiafrank.web.model.Resource;
import com.asiafrank.web.vo.ResourceVO;

public interface ResourceBO extends BO<Resource, ResourceVO, java.lang.Integer> {
}
