package com.asiafrank.web.bo;

import com.calanger.common.bo.BO;
import com.asiafrank.web.model.OperatorLog;
import com.asiafrank.web.vo.OperatorLogVO;

public interface OperatorLogBO extends BO<OperatorLog, OperatorLogVO, java.lang.Integer> {
}
