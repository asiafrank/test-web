package com.asiafrank.web.bo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calanger.common.bo.AbstractBO;
import com.calanger.common.dao.DAO;
import com.asiafrank.web.bo.ResourceBO;
import com.asiafrank.web.dao.ResourceDAO;
import com.asiafrank.web.model.Resource;
import com.asiafrank.web.vo.ResourceVO;

@Service("resourceBO")
public class DefaultResourceBO extends AbstractBO<Resource, ResourceVO, java.lang.Integer> implements ResourceBO {
    @Autowired
    private ResourceDAO resourceDAO;

    @Override
    protected DAO<Resource, ResourceVO, java.lang.Integer> getDAO() {
        return resourceDAO;
    }
}
