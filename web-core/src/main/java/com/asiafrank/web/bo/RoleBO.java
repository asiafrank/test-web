package com.asiafrank.web.bo;

import com.calanger.common.bo.BO;
import com.asiafrank.web.model.Role;
import com.asiafrank.web.vo.RoleVO;

public interface RoleBO extends BO<Role, RoleVO, java.lang.Integer> {
}
